﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Pelilauta.GameLogic
{
    public class BoardRulesAndUtilities
    {
        public const int ROW_CHARACTER = 0;
        public const int COLUMN_CHARACTER = 1;

        public const int COLUMNS = 10;
        public const int ROWS = 10;


        /// <summary>
        /// Returns the index associated with alphabet running in order
        /// </summary>
        /// <param name="alphabet">Small alphabet character, expected to be in a-z spectrum with a resulting in index of 0 and c in 2</param>
        /// <returns></returns>
        public static int CalculateIndexFromAlphabet(char alphabet)
        {
            return (int)(alphabet - 'a');
        }
    }
}
