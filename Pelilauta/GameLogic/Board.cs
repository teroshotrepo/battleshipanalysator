﻿using System;
using System.Reflection;
using System.Text;

namespace Pelilauta.GameLogic
{
    public class Board
    {
        private Board.Location[,] gameBoard;
        public char ship = '*';
        public char missedShot = 'o';
        public char shotShip = 'X';

        public int Rows
        {
            get
            {
                return gameBoard.GetLength(0);
            }
        }

        public int Columns
        {
            get
            {
                return gameBoard.GetLength(1);
            }
        }


        public enum Location
        {
            Nothing,
            Ship,
            MissedShot,
            ShotShip
        }


        public Board(int rows, int columns)
        {
            gameBoard = new Board.Location[rows, columns];
        }


        private Board.Location this[int x, int y]
        {
            get
            {
                SafeguardBoardIndexAccess(x, y);
                return gameBoard[y, x];
            }
            set
            {
                SafeguardBoardIndexAccess(x, y);
                gameBoard[y, x] = value;
            }
        }

 
        /// <summary>
        /// Can be used anywhere to throw an exception if the provided x or y are not properly useable with the board
        /// </summary>
        /// <param name="x">x-coordinate</param>
        /// <param name="y">y-coordinate</param>
        private void SafeguardBoardIndexAccess(int x, int y)
        {
            if (x < 0 || y < 0)
            {
                throw new GameLogicException("X or Y cannot be negative when accessing the board!");
            }
            if (x >= Columns)
            {
                throw new GameLogicException(String.Format("Accessing board column index with value of {0} when the size was only {1}", x, Columns));
            }
            if (y >= Rows)
            {
                throw new GameLogicException(String.Format("Accessing board row index with value of {0} when the size was only {1}", y, Rows));
            }
        }


        public void DeployShip(Ship ship)
        {
            for (int i = 0; i < ship.Count; i++)
            {
                var coordinate = ship[i];
                this[coordinate.X, coordinate.Y] = Board.Location.Ship;
            }
        }


        public bool Shoot(int x, int y)
        {
            Board.Location shotSquare = this[x, y];

            switch (shotSquare)
            {
                case Board.Location.Ship:
                    this[x, y] = Board.Location.ShotShip;
                    return true;
                case Board.Location.ShotShip:
                    return true;
                //For claritys sake, in this occasion its nice to list the other possibilities; they will fall to default regardless without listing.
                case Board.Location.MissedShot:
                case Board.Location.Nothing:
                default:
                    this[x, y] = Board.Location.MissedShot;
                    return false;
            }
        }


        public char ToBoardCharacter(Board.Location loc)
        {
            switch (loc)
            {
                case Board.Location.Ship:
                    return ship;
                case Board.Location.MissedShot:
                    return missedShot;
                case Board.Location.ShotShip:
                    return shotShip;
                default:
                    return ' ';
            }
        }


        public string ToString(char borderChar, char separator)
        {
            StringBuilder sb = new StringBuilder();
            string border = new String(borderChar, Columns * 2 + 1);

            sb.AppendLine(border);
            //This 'for' is still a bit hard to read
            for (int y = 0; y < Rows; y++)
            {
                sb.Append(separator);
                for (int x = 0; x < Columns; x++)
                {
                    sb.Append(ToBoardCharacter(this[x, y]));
                    sb.Append(separator);
                }
                sb.AppendLine();
                sb.AppendLine(border);
            }

            return sb.ToString();
        }


        public override string ToString()
        {
            return ToString('-', '|');
        }
    }
}


