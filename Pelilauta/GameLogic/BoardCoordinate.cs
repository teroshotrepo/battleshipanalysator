﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Pelilauta.GameLogic
{
    public class BoardCoordinate
    {
        private int y, x = -1;
        public int Y { get => y; }
        public int X { get => x; }

        public BoardCoordinate(string coordinate)
        {
            if (coordinate.Length != 2)
            {
                throw new GameLogicException(String.Format("The ship coordinate '{0}' was not in a valid format", coordinate));
            }

            char rowCharacter = coordinate[BoardRulesAndUtilities.ROW_CHARACTER];
            char columnCharacter = coordinate[BoardRulesAndUtilities.COLUMN_CHARACTER];

            this.y = BoardRulesAndUtilities.CalculateIndexFromAlphabet(rowCharacter);

            if (!int.TryParse(columnCharacter.ToString(), out int x))
            {
                throw new GameLogicException(String.Format("Column character was not a number in {0}!", coordinate));
            }
            //From ordinary to index
            this.x = x - 1;
        }


        public BoardCoordinate(int x, int y)
        {
            this.x = x;
            this.y = y;
        }

        
    }
}
