﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Pelilauta.GameLogic
{
    public class Ship
    {
        private List<BoardCoordinate> coordinates = new List<BoardCoordinate>();

        public Ship(List<string> stringCoordinates)
        {
            foreach (string coordinateString in stringCoordinates)
            {
                coordinates.Add(new BoardCoordinate(coordinateString));
            }
        }


        public int Count
        {
            get
            {
                return coordinates.Count;
            }
        }


        public BoardCoordinate this[int index]
        {
            get
            {
                return coordinates[index];
            }
            //Is there need for more access than private?
            private set
            {
                coordinates[index] = value;
            }
        }


        public void AddCoordinate(BoardCoordinate coordinate)
        {
            this.coordinates.Add(coordinate);
        }
    }
}
