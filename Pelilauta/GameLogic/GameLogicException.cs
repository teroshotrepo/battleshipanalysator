﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Pelilauta.GameLogic
{
    public class GameLogicException : Exception
    {
        public GameLogicException(string message) : base(message)
        {
        }
    }
}
