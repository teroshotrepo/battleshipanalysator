﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Pelilauta.Data
{
    //Thank you once again http://json2csharp.com/ for quick help with newtonsoft
    public class GameData
    {
        public List<List<string>> nappulat { get; set; }
        public List<string> shotsFired { get; set; }

    }
}
