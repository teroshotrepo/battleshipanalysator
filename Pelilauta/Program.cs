﻿using Pelilauta.Data;
using Pelilauta.GameLogic;
using Pelilauta.Tools;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Net.Http.Headers;
using System.Threading.Tasks;


namespace Pelilauta
{
    public class Program
    {
        public static void Main(string[] args)
        {
            //TODO: The row and column could be some data loaded from different file or from the 'sotatanner'
            var board = new Board(BoardRulesAndUtilities.ROWS, BoardRulesAndUtilities.COLUMNS);

            string filename = System.IO.Path.Combine("Data", "sotatanner.json");

            GameData gameData = FileUtilities.ReadGameData<GameData>(filename);
            List<Ship> ships = new List<Ship>();

            foreach (var shipStringCoordinates in gameData.nappulat)
            {
                ships.Add(new Ship(shipStringCoordinates));
            }

            // DEPLOY ZE ARMAAADA!
            foreach (var ship in ships)
            {
                board.DeployShip(ship);
            }

            string[,] shotMatrix = ArrayUtilities.ConvertListToMatrix<string>(gameData.shotsFired, 10, 10);

            for (int y = 0; y < shotMatrix.GetLength(0); y++)
            {
                for (int x = 0; x < shotMatrix.GetLength(1); x++)
                {
                    if(!string.IsNullOrEmpty(shotMatrix[y, x]))
                    {
                        board.Shoot(x, y);
                    }
                }
            }

            DrawBoard(board);
            Console.ReadKey();
        }


        public static void DrawBoard(Board board)
        {
            Console.WriteLine("Post Game Analysis :");

            Console.Write(board.ToString());

            Console.WriteLine("* = Boatpiece, o = missed shot, X = DIRECT HIT!");
        }
    }
}

