﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Pelilauta.Tools
{
    public class FileUtilities
    {
        public static T ReadGameData<T>(string filename)
        {
            if (System.IO.File.Exists(filename))
            {
                return Newtonsoft.Json.JsonConvert.DeserializeObject<T>(System.IO.File.ReadAllText(filename));
            }
            else
            {
                throw new System.IO.FileNotFoundException("File with the given filename could not be loaded from the system", filename);
            }
        }
    }
}
