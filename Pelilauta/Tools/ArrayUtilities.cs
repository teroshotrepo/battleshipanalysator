﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Pelilauta.Tools
{
    public class ArrayUtilities
    {
        public static T[,] ConvertSingleArrayToMatrix<T>(T[] array, int rows, int columns)
        {
            T[,] matrix = new T[rows, columns];

            for (int i = 0; i < array.Length; i++)
            {
                matrix[i / rows, i % columns] = array[i];
            }

            return matrix;
        }


        public static T[,] ConvertListToMatrix<T>(List<T> list, int rows, int columns)
        {
            T[,] matrix = new T[rows, columns];

            for (int i = 0; i < list.Count; i++)
            {
                matrix[i / rows, i % columns] = list[i];
            }

            return matrix;
        }

    }
}
